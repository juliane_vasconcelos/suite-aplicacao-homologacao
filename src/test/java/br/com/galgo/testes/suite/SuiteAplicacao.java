package br.com.galgo.testes.suite;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.testes.TesteConectividadeWS;
import br.com.galgo.testes.TesteFundosWS;
import br.com.galgo.testes.TestePLCotaWS;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaCompromisso;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaEntidade;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaLog;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaPLCota;
import br.com.galgo.testes.recursos_comuns.teste.TesteConsultaFundos;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({//
TesteConsultaEntidade.class,//
		TesteConsultaCompromisso.class,//
		TesteConsultaLog.class,//
		TesteConsultaPLCota.class,//
		TesteConsultaFundos.class, //
		TesteFundosWS.class,//
		TestePLCotaWS.class, //
		TesteConectividadeWS.class //
})
public class SuiteAplicacao {

	private static final String PASTA_SUITE = "Aplicação";

	@BeforeClass
	public static void setUp() throws Exception {
		SuiteUtils.configurarSuiteDefault(Ambiente.HOMOLOGACAO, PASTA_SUITE);
	}

}
